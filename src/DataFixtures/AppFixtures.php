<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Animal;
use App\Entity\Cart;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Service\UploadService;
use Symfony\Component\Filesystem\Filesystem;

class AppFixtures extends Fixture
{
    private $encoder;
    private $uploader;
    private $filesystem;

    public function __construct(UserPasswordEncoderInterface $encoder, UploadService $uploader, Filesystem $filesystem)
    {
        $this->encoder = $encoder;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
    }

    public function load(ObjectManager $manager)
    {

        $this->filesystem->remove($_ENV['UPLOAD_DIRECTORY']);

        //On fait une petite boucle pour ajouter des post à 5 users

        //On chope la référence à chaque user sur l'autre fixture
        $panier = new Cart();
        $manager->persist($panier);

        $userjj = new User();
        $userjj->setuserName('janjak');
        $userjj->setEmail('jijidu23@gmail.com ');
        $userjj->setAddress('3 rue paul eluard');
        
        $userjj->setFirstName('jeanjacques');
        $userjj->setRoles('ROLE_ADMIN');
        $userjj->setName('granet');
        $password = $this->encoder->encodePassword($userjj, '12345');
        $userjj->setPassword($password);
        $userjj->setPhoneNumber('0610105387');
        $userjj->setCart($panier);
        $userjj->setImage('http://a51.idata.over-blog.com/0/34/77/77/uzoo106.jpg');
        $manager->persist($userjj);

        // $product = new Product();
        // $manager->persist($product);
        // $user1 = new User();
        // $user1->setuserName('nolan');
        // $password = $this->encoder->encodePassword($user1, '12345');
        // $user1->setPassword($password);

        // $user1->setAddress('3 rue paul eluard');
        // $user1->setFav(['lapin', 'brebis', 'ours vietnamien du pakistan']);
        // $user1->setIdCart(42);
        // $user1->setRoles('ROLE_ADMIN');
        // $manager->persist($user1);

        $animal1 = new Animal();
        $animal1->setRace('Doggybird');
        $animal1->setPrice(250);
        $animal1->setPhone(6101053871);
        $animal1->setDescription("Ce magnifique Doggybird vous apportera amour et tendresse ainsi que joie et prospérité. Au Japon, cet animal porte chance quand vous le croisez à l'aube.");
        $animal1->setAge(1);
        $animal1->setImage("http://media.topito.com/wp-content/uploads/2014/01/2ef7525c-245c-4cd6-9d39-f30e55acc8e2_Lab-Dird.png");
        $animal1->setAddress('Tokyo, Japon');
        $manager->persist($animal1);

        $animal2 = new Animal();
        $animal2->setRace('Pluméon à clochette');
        $animal2->setPrice(25);
        $animal2->setPhone(6101053872);
        $animal2->setDescription("Ce magnifique Pluméon à clochette vient tout droit de Moldavie ! Son poil doux semblable à de douces plumes comblera les plus attendris d'entre vous !");
        $animal2->setAge(5);
        $animal2->setImage("https://randommization.com/wp-content/uploads/2011/02/weird-animals.jpg");
        $animal2->setAddress('Moldavie');
        $manager->persist($animal2);

        $animal3 = new Animal();
        $animal3->setRace('Appaloosa à bec long');
        $animal3->setPrice(2000);
        $animal3->setPhone(6101053873);
        $animal3->setDescription("Ce cheval est un Appaloosa à bec long, un animal si rare que vous ne le verrez sûrement qu'une seule fois dans votre vie à part si vous l'adoptez et que vous le fassiez reproduire ! Propagez cette espèce autour de vous. Le saviez-vous? Le Appaloosa à bec long est hermaphrodite.");
        $animal3->setAge(5);
        $animal3->setImage("http://arcus.a.r.pic.centerblog.net/o/2aecac9a.jpg");
        $animal3->setAddress('La plaine');
        $manager->persist($animal3);

        $animal4 = new Animal();
        $animal4->setRace('Lion hamsterien');
        $animal4->setPrice(2500);
        $animal4->setPhone(6101053874);
        $animal4->setDescription("Vous pensiez la légende du lion hamstèrien inventée et entièrement basée sur des mensonges ? Vous avez la preuve que non avec Ricchie, un jeune lion hamstérien ! Cette espèce se nourrit principalement de bovidés comme les élans ou les gnous, et de mammifères comme les buffles et les phacochères. Mais la liste est longue...
        Bien que l’animal consomme en moyenne 7 kg de viande par jour, le lion ne passe pas son temps à chasser. En effet, le prédateur demeure inactif pendant plus de 21 heures par jour. Ce grand félin ne chasse qu’une fois sa réserve de nourriture épuisée. Le lion hamstèrien vit environ 15 ans en pleine nature et jusqu'à 30 ans en captivité.");
        $animal4->setAge(5);
        $animal4->setImage("https://cdn-images-1.medium.com/max/1600/1*mKsG7d5tm08Ng1nZ3XH-gw.jpeg");
        $animal4->setAddress('64 Rue du zoo, rue des animaux');
        $manager->persist($animal4);

        $animal5 = new Animal();
        $animal5->setRace('Macaque double face');
        $animal5->setPrice(25);
        $animal5->setPhone(6101053875);
        $animal5->setDescription("Non vous ne rêvez pas, c'est bien un splendide macaque double face que vous avez-là. Cet animal a doublement besoin d'affection, donc adoptez-le.");
        $animal5->setAge(5);
        $animal5->setImage("https://www.2tout2rien.fr/wp-content/uploads/2018/01/Recurrence-des-animaux-a-peine-bizarre-par-Milan-Racmolnar-1-singe.jpg");
        $animal5->setAddress('Dans un arbre');
        $manager->persist($animal5);

        $animal6 = new Animal();
        $animal6->setRace('Hélico biche');
        $animal6->setPrice(25);
        $animal6->setPhone(6101053876);
        $animal6->setDescription("Héllie, une jolie Hélico biche, attend un nouveau maître !");
        $animal6->setAge(5);
        $animal6->setImage("https://i.skyrock.net/5556/63025556/pics/2535405591_1.gif");
        $animal6->setAddress('Dans le ciel');
        $manager->persist($animal6);

        $panier = new Cart();
        $panier->setCartPrice(50);
        $manager->persist($panier);

        $manager->flush();
    }
}
