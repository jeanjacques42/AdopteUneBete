<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\AnimalRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Entity\Animal;



class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function Accueil(AnimalRepository $animal)
    {
        $user = $this->getUser();
        return $this->render('accueil/index.html.twig', [
            'animals' => $animal->findAll(),
            'user' => $user
        ]);
    }

    /**
     * @Route ("/announce/{id}", name="announce")
     */
    public function announce(AnimalRepository $animal, int $id)
    {
        return $this->render("accueil/announce.html.twig", ["announce" => $animal->find($id)]);
    }
    /**
     * @Route("/favoris", name="favoris")
     */
    public function Favoris(UserRepository $userRepository)
    {
        return $this->render('accueil/favoris.html.twig', [

        ]);
    }
  
    
    // }
    /**
     * @Route("/addPanier/{animal}", name="addPanier")
     */
    public function addPanier(ObjectManager $manager, AnimalRepository $repo, Animal $animal)
    {
        $cart = $this->getUser()->getCart();
        $cart->addAnimal($animal);
        $manager->persist($cart);
        $manager->flush();
        dump($cart);



        return $this->render("accueil/announce.html.twig", ["announce" => $animal]);
        // $cart->addAnimal($animal);

    }

    /**
     * @Route("/deletePanier/{animal}", name="deletePanier")
     */
    public function deletePanier(ObjectManager $manager, AnimalRepository $repo, Animal $animal)
    {
        $cart = $this->getUser()->getCart();
        $cart->removeAnimal($animal);
        $manager->persist($cart);
        $manager->flush();
        dump($cart);

        return $this->render("panier/panier.html.twig", [
            "animals" => $cart->getAnimal()
            ]);
        // $cart->addAnimal($animal);
    }


    /**
     * @Route("/panier/{user}", name="panier")
     */
    public function panier(
        User $user
    ) {

        // dump($user);
        $cart = $user->getCart();
        $cartAnimal = $cart->getAnimal();

        return $this->render('panier/panier.html.twig', [
            'animals' => $cartAnimal
        ]);
    }
    /**
     * @Route("/compte", name="compte")
     */
    public function MonCompte()
    {
        return $this->render('compte/compte.html.twig');
    }
    /**
     * @Route("/filtre/{max}", name = "filtre")
     */
    public function filtre(AnimalRepository $animRepo, string $max) //injecte la Request
    {
        //Récupérer sur la request le champ du formulaire avec un get(le name de l'input)
        // $price = $animRepo->findByPrice($request->get('maxPrice'));
        // $request->getBasePath('search');
        if ($max == 'maxPrice') {
            return $this->render('accueil/index.html.twig', [
                'animals' => $animRepo->findBy([], ['price' => 'DESC'])
            ]);
        } else {
            return $this->render('accueil/index.html.twig', [
                'animals' => $animRepo->findBy([], ['price' => 'ASC'])
            ]);
        }


        //Utiliser la variable obtenue pour faire un findBy en utilisant le repository        

        // return $this->render('search/filtre.html.twig', [
        //     'price' => $price
        // ]);
    }
}
