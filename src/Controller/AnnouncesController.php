<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\AnnouncesType;
use App\Entity\Animal;
use App\Entity\User;
use App\Service\UploadService;

class AnnouncesController extends AbstractController
{

    /**
     * @Route("/announces", name="announces")
     */
    public function announces(
        Request $request,
        ObjectManager $manager
    ) {

        $animal = new Animal();
        $form = $this->createForm(AnnouncesType::class, $animal);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($animal);
            $manager->flush();

            return $this->redirectToRoute('accueil');
        }

        return $this->render('form/announces.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/add-favoris/{user}/{animal}", name="add_favoris")
     */
    public function addFavoris(User $user, Animal $animal, ObjectManager $manager, Request $request)
    {

        //choper le user actuel
        // $manager
        // ->addFav($animal->find($id));


        //faire un addFav
        $user->addFavori($animal);
        $manager->persist($user);

        // $manager->persist(add($animal));
        $manager->flush();
        dump($animal);
        dump($user);


        // return $this->redirectToRoute($request->getBasePath());
        //return $this->redirectToRoute('announce', ['id' => $animal->getId()]);
        // return $this->redirectToRoute('accueil');
        return $this->render("accueil/announce.html.twig", ["announce" => $animal]);
    }

    /**
     * @Route("/delete/{id}", name = "delete")
     */
    public function deleteFavoris(Animal $animal, ObjectManager $manager)
    {

        $this->getUser()->removeFavori($animal);
        $manager->flush();
        return $this->redirectToRoute('favoris');
    }

    /**
     * @Route("/modify-announce/{animal}", name = "modify_announce")
     */
    public function modifyAnnounce(
        Request $request,
        ObjectManager $manager,
        UploadService $service,
        Animal $animal
    ) {
        dump($animal);
        $user = $this->getUser();

        if ($user != $this->getUser()) {
            return new Response('Vous n\'avez pas la permission de modifier cette annonce, 401');
        }
        $form = $this->createForm(AnnouncesType::class, $animal);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * Lorsqu'on enregistre un user, il faut encoder son mot de
             * passe en utilisant le UserPasswordEncoderInterface
             */
            //Puis remplacer son mot de passe en clair par le mot de passe hashé
            if ($user->getRoles()[0] === 'ROLE_ADMIN') {
                $user->setRoles('ROLE_ADMIN');
            } else {
                $user->setRoles('ROLE_USER');
            }

            $manager->persist($animal);
            $manager->flush();
            return $this->redirectToRoute('accueil');
        }

        return $this->render('form/announces.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
