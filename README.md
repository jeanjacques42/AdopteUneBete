# Adopte-Une-Bête

## Description

Adopte-Une-Bête est un site Web conçu pour permettre aux animaux qui sortent du commun d'avoir la lueur d'espoir d'être adoptés. Les utilisateurs peuvent trouver des animaux de hors pairs dotés de dons exceptionnels qui apporteront joie et bonne hummeur en leur compagnie. 

### Les contributeurs et leurs rôles principaux

#### Jean-Jaques - Developpeur

* Co-développeur principal

* Créer un formulaire d'inscription/connexion correspondante avec la BDD

* Organisation du tableau de bord Git 



#### Aissata - Développeuse

* Faire l'affichage de la page d'accueil version-1 du site

* Ajouter un animal à ses favoris

* Intégration de la barre de recherche et filtre

* Mise à jour du README

* organisation du tableau de bord Git

#### Nolan - Developpeur

* Créer toutes les entités (User, Animal, Cart)

* Créer un formulaire d'ajout d'animal pour admin

* CRUD admin pour les annonces disponibles

* Style CSS/Bootstrap du site en général

* organisation du tableau de bord Git

 

### Technologies et ressources utilisées

* Symfony 4.2

* Doctrine

* SQL

* Twig

* CSS

* Bootstrap

* Bootswatch

* Git/Gitlab

### Wireframe (premières implémentations/prototypes)


### Captures d'écran du site

![20% center](./public/photos/acceuil.png)

![20% center](./public/photos/connection.png)

![20% center](./public/photos/mes-favoris.png)

### Nos difficultées

* Jean-Jacques:

* Aissata: j'ai eu des difficultées avec les relations ManyToMany entre le user et les articles

* Nolan: J'ai eu beaucoup de mal a faire quelque chose de responsive et j'ai passé beaucoup trop de temps à faire du CSS inutile.

### Ce qu'on a aimé !

* Nolan: J'ai adoré la thématique que l'on a pris. L'entente dans le groupe était superbe et nous avons rencontrés que très peu de difficultés ! 
Sur des points un petit peu plus techniques, gérer le CRUD et l'affichage de son template est pour moi un vrai plaisir ! Je suis content d'avoir façilement réussi à faire la partie administrateur de notre site !

* Jean-Jaques:

* Aissata: Ce que j'ai aimé c'est que ce projet m'a permi de mieux comprendre symfony ainsi que git. Ce que j'ai le PLUS aimé, c'est la superbe entente qu'il y avait entre mes collègues et moi.


### Hommage à Mélissa, notre ange partit trop tôt...

Mélissa, te savoir dans notre groupe était une joie,
Sur laquelle je ne peux poser les mots.
Malheureusement le destin a été cruel avec toi,
Et ce fut la fin de notre trio...

Nous n'oublierons jamais ces quelques secondes passées à tes côtés,
Mélissa, nous te gardons dans notre coeur à jamais.

https://www.youtube.com/watch?v=RgKAFK5djSk

![20% center](./public/photos/Mel.jpg)
